# NannouExamples

Random examples created while playing with the Nannou crate for Rust. 

Those examples starting with Shiffman_ are taken from Daniel Shiffman's book, [The Nature of Code](https://natureofcode.com/)