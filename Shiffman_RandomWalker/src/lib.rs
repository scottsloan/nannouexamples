use rand::Rng;
use nannou::prelude::*;

pub struct Walker {
    pub last_point : Point2,
    pub cur_point : Point2,
    pub step_size :f32
}

impl Walker {
    
    pub fn new() -> Walker {
        Walker {
            last_point : pt2(0.0, 0.0),
            cur_point : pt2(0.0, 0.0),
            step_size : 1.0
        }
    }

    pub fn Step(&mut self)
    {
       // Save our last point
       let mut _x: f32 = self.cur_point.x;
       let mut _y: f32 = self.cur_point.y;

       self.last_point = pt2(_x as f32, _y as f32);
   
       //Calculate our next steps
       let choice:i32 = rand::thread_rng().gen_range(0..4);
   
       match choice {
           0 => _x = _x + self.step_size,
           1 => _x = _x - self.step_size,
           2 => _y = _y + self.step_size,
           3 => _y = _y - self.step_size,
           _ => _x = _x
       }
       
       // Update our current Point
       self.cur_point = pt2(_x,_y);
    }

    pub fn Step_Multi_Direction(&mut self)
    {
        let mut _x: f32 = self.cur_point.x;
        let mut _y: f32 = self.cur_point.y;
    
        self.last_point = pt2(_x as f32, _y as f32);
    
        //Calculate our next steps
        let x_step : i32 = rand::thread_rng().gen_range(-1..2);
        let y_step : i32 = rand::thread_rng().gen_range(-1..2);
    
        _x = _x + x_step as f32;
        _y = _y + y_step as f32;
    
        // Update our current Point
        self.cur_point = pt2(_x,_y);
    }

}