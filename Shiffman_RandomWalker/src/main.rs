use nannou::prelude::*;
use Shiffman_RandomWalker::Walker;

fn main() {
    nannou::app(model).update(update).run();
}

fn model(_app: &App) -> Walker {

    _app.new_window().size(640, 360)
                    .view(view)
                    .build()
                    .unwrap();

    Walker::new()
}

fn update(_app: &App, _model: &mut Walker, _update: Update) {
    //_model.Step();
    _model.Step_Multi_Direction();
}

fn view(_app: &App, _model: &Walker, _frame: Frame) {

    let draw = _app.draw();

    draw.line()
            .start(_model.last_point)
            .end(_model.cur_point)
            .weight(4.0)
            .color(STEELBLUE);

    draw.to_frame(_app, &_frame).unwrap();
}
